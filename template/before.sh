#!/usr/bin/env bash

# Export the module function if it exists
[[ $(type -t module) == "function" ]] && export -f module

# Setup TMPDIR
TMPDIR="$(mktemp -d)"
export TMPDIR

# Fix XDG Run Time
XDG_RUNTIME_DIR="$TMPDIR"
export XDG_RUNTIME_DIR

# Load Form Information
OOD_SR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
if [ -f "${OOD_SR}/form.sh" ]; then
	# shellcheck disable=SC1090,SC1091
	source "${OOD_SR}/form.sh"
fi
unset OOD_SR
