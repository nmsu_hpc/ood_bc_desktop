#!/usr/bin/env bash

# Ensure modules are set to default
module -q reset

# CD Into Home Directory
cd "$HOME" || true

# Ensure that the user's configured login shell is used
SHELL="$(getent passwd "$USER" | cut -d: -f7)"
export SHELL

# fix dbus issues with RHEL 8
unset DBUS_SESSION_BUS_ADDRESS

# Start Desktop
echo "Launching Deskop '$OOD_DESKTOP' ..."
# shellcheck disable=SC1090
source "${OOD_STAGED_ROOT}/desktops/${OOD_DESKTOP}.sh" "${OOD_STAGED_ROOT}/desktops/configs"
echo "Deskop '$OOD_DESKTOP' endec ..."
